/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StatusBar,
} from 'react-native';

import Dashboard from './src/Screens/Dashboard'
const App: () => React$Node = () => {
  return (
    <>
      {/* <StatusBar barStyle="dark-content" /> */}
      <SafeAreaView style={{ flex: 1,height: "100%" }}>
        <Dashboard />
      </SafeAreaView>
    </>
  );
};

export default App;
