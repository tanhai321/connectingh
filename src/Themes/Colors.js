/**
 * @providesModule Colors
 */
const colors = {
  bgNav: "rgb(76,171,242)", // "rgba(76,171,242,0.15)"
  bgMain: "rgb(255,255,255)",

  bgSecond: "rgb(228,242,253)",
  bgSecond2: "rgb(253, 227, 227)",
  bgItem: "rgb(228,242,253)",
  bgInput: "rgb(255,255,255)",
  bgModal: "rgb(255,255,255)",
  bgTouchAll: "#0FA4EA",
  bgTouchReceipt: "rgb(242,162,76)",
  bgTouchNailJob: "rgb(99,207,165)",
  bgTouchTravel: "rgb(242,110,110)",
  bgAll: "rgb(241,248,254)",
  bgReciept: "rgb(255,247,247)",
  bgPlaceHoder: "#a6a6a6",
  //

  bgItemSign: "rgba(255,255,0,0.2)",
  bgItemSer: "rgba(76,171,242,0.15)",
  bgItemDone: "rgba(0,255,0,0.15)",
  //
  bgIcon: "rgb(76,171,242)",
  bgIconSpecial: "rgb(227,80,80)",

  backdropColor: "rgba(0,0,0,0.5)",
  backdropColor2: "rgba(0,0,0,0.2)",

  border: "rgb(76,171,242)",
  borderGroup: "rgb(76,171,242)",

  line: "rgba(0,0,0,0.1)",
  lineSpecial: "rgb(227,80,80)",
  lineBorder: "rgb(76,171,242)",
  lineBackground: "#585C88",

  btn: "rgb(76,171,242)",
  btnDisable: "rgb(170,170,170)",
  btnEnable: "rgb(76,171,242)",

  textNav: "#FFFFFF",
  textHeader: "#FFFFFF",
  textFooter: "#FFFFFF",
  textMenu: "#FFFFFF",

  textGroupSecond: "#FFFFFF",
  textCopyright: "#808080",
  textButton: "#FFFFFF",
  textInput: "rgb(85,85,85)",
  textContent: "rgb(85,85,85)",
  textPlaceHolder: "rgb(170,170,170)",
  textActive: "rgb(76,171,242)",
  textDisable: "rgb(170,170,170)",
  textCancel: "rgb(227,80,80)",

  icon: "#FFFFFF",
  iconInput: "rgb(170,170,170)",
  iconNav: "#FFFFFF",
  iconChecked: "rgb(76,171,242)",
  iconCancel: "rgb(227,80,80)",
  iconPayment: "rgb(100,240,0)",

  pickerBtn: [18, 142, 237, 1],
  pickerHeader: [185, 220, 249, 1],
  pickerContent: [228, 242, 253, 1],

  spin: "#fff",
  serviceAddOn: "#32CD32",
  phoneNormal: "rgb(76,171,242)",
  phoneCalling: "rgb(0,240,0)", // fix
  phoneOff: "rgb(227,80,80)"
};
export default colors;
