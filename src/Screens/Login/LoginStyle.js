import { StyleSheet } from "react-native";
import { Layout } from "../../Utilities/Layout";
import { Colors } from "../../Themes";
const ss = StyleSheet.create({
    container: {
        flex: 1,
    },
    logoContainer: {
        height: Layout.minerScale(600),
        justifyContent: 'center',
        alignItems: 'center',

    },
    sizeIcon: {
        fontSize: Layout.minerScale(130)
    },
    iconsLogo: {
        flexDirection: 'row',
        height: Layout.minerScale(550),
        width: Layout.minerScale(550),
        borderColor: Colors.bgTouchAll,
        borderRadius: Layout.minerScale(550) / 2,
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    formLogin: {
        height: Layout.minerScale(700),
        justifyContent: 'space-evenly',
        alignItems: "center"
    },
    textInput: {
        width: Layout.minerScale(800),
        borderColor: Colors.bgTouchAll,
        borderBottomWidth: 1,
        height: Layout.minerScale(150),
    },
    button: {
        marginTop: Layout.minerScale(80),
        borderRadius: Layout.minerScale(120) / 2,
        width: Layout.minerScale(800),
        height: Layout.minerScale(120),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.bgTouchAll
    },
    textNetwork: {
        marginLeft: Layout.minerScale(50),
        fontWeight: "bold",
        fontSize: Layout.minerScale(60),
        marginVertical: Layout.minerScale(30)
    },
    touchWifi: {
        width: Layout.minerScale(600),
        height: Layout.minerScale(120),
        paddingHorizontal: Layout.minerScale(20)
        // marginRight: Layout.minerScale(120),
    },
    textSsid: {
        textDecorationLine: 'underline'
        // fontSize: Layout.minerScale(60)
    }
});

export default ss;