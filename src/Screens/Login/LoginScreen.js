import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, FlatList, PermissionsAndroid } from 'react-native';
import wifi from 'react-native-android-wifi';
import { Layout } from '../../Utilities/Layout'
import MateIcon from 'react-native-vector-icons/MaterialIcons'
import AntIcon from 'react-native-vector-icons/AntDesign'
import ss from './LoginStyle'
import { Colors } from '../../Themes';
import InputIcon from '../../Components/InputIcon'

export default class LoginScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ssid: "",
            password: "",
            listWifi: [],
            refreshListWifi: false,
        };
    }

    async componentDidMount() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    'title': 'Wifi networks',
                    'message': 'We need your permission in order to find wifi networks'
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("Thank you for your permission! :)");
            } else {
                console.log("You will not able to retrieve wifi available networks list");
            }
        } catch (err) {
            console.warn(err)
        }
    }

    componentWillMount() {
        this.getListWifi();
    }

    getListWifi = () => {
        wifi.loadWifiList((wifiStringList) => {
            let wifiArray = JSON.parse(wifiStringList);
            this.setState({
                listWifi: wifiArray
            })
        },
            (error) => {
                console.log(error);
            }
        );
    }

    render() {
        return (
            <View style={ss.container}>
                <View style={ss.logoContainer}>
                    <View style={ss.iconsLogo}>
                        <AntIcon
                            color={Colors.bgTouchAll}
                            style={ss.sizeIcon}
                            name={"wifi"}
                        />
                        <MateIcon
                            color={Colors.bgTouchTravel}
                            size={Layout.minerScale(250)}
                            name={"location-on"}
                        />
                        <MateIcon
                            color={Colors.bgTouchAll}
                            style={ss.sizeIcon}
                            name={"bluetooth"}
                        />
                    </View>
                </View>
                <View style={ss.formLogin}>
                    <View style={{ height: Layout.minerScale(100) }}>
                        <TextInput
                            value={this.state.ssid}
                            placeholder={"HotspotID or WIFI"}
                            onChangeText={(textSsid) => this.setState({
                                ssid: textSsid
                            })}
                            style={ss.textInput}
                        />
                    </View>
                    <View style={{ height: Layout.minerScale(100) }}>
                        <InputIcon
                            value={this.state.password}
                            placeholder={"Password"}
                            onChangeText={(password) => this.setState({
                                password: password
                            })}
                            password={true}
                            style={ss.textInput}
                        />
                    </View>
                    <TouchableOpacity style={ss.button}>
                        <Text style={{
                            fontSize: Layout.minerScale(60),
                            fontWeight: "bold",
                            color: Colors.textNav
                        }}>Connect</Text>
                    </TouchableOpacity>
                </View>
                <View style={{
                    flex: 1,
                }}>
                    <Text style={ss.textNetwork}>Networks: </Text>
                    <FlatList
                        style={{ marginBottom: Layout.minerScale(50) }}
                        contentContainerStyle={{
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                        keyExtractor={(item, index) => index.toString()}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        numColumns={2}
                        refreshing={this.state.refreshListWifi}
                        onRefresh={this.getListWifi}
                        data={this.state.listWifi}
                        renderItem={({ item, index }) => {
                            return <TouchableOpacity style={ss.touchWifi}>
                                <Text
                                    numberOfLines={1}
                                    style={ss.textSsid}>
                                    {item.SSID}
                                </Text>
                            </TouchableOpacity>
                        }}
                    />
                </View>
            </View>
        );
    }
}
