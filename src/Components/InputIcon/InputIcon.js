import { bool, string, func } from "prop-types";
import React, { PureComponent } from "react";
import { TextInput, View, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import { Colors, Typography } from "../../Themes";
export default class InputIcon extends PureComponent {
  static propTypes = {
    password: bool,
    value: string,
    nameIcon: string,
    placeholder: string,
    onChangeText: func,
    inputRef: func,
    secureTextEntry: bool,
    numeric: bool
  };

  static defaultProps = {
    password: false,
    value: "",
    nameIcon: "",
    placeholder: "",
    onChangeText: () => { },
    inputRef: () => { },
    secureTextEntry: false,
    numeric: false
  };
  constructor(props) {
    super(props);
    this.state = {
      secureTextEntry: this.props.secureTextEntry
    };
  }

  validatePassword = pass => {
    let temp = pass;
    temp = temp.replace(/\s/g,'');
    // temp = temp.replace(/[^a-zA-Z0-9 ]/g, "")
    return temp;
  }

  onChangeText = (text) => {
    if (this.props.password) {
      if (this.validatePassword(text) === text) {
        this.props.onChangeText(text);
      }
    }
    else {
      this.props.onChangeText(text);
    }
  }
  render() {
    return (
      <View style={[styles.inputWraper, this.props.style]}>
        <Icon
          name={this.props.nameIcon}
          // style={[styles.icon, this.props.styleIcon]}
          size={this.props.size}
          color={this.props.color}
        />
        <TextInput
          ref={input => this.props.inputRef(input)}
          onChangeText={(text) => this.onChangeText(text)}
          style={styles.textInput}
          value={this.props.value}
          keyboardType={this.props.numeric ? "numeric" : "default"}
          placeholder={this.props.placeholder}
          autoCapitalize={"none"}
          underlineColorAndroid="transparent"
          secureTextEntry={this.state.secureTextEntry}
          onSubmitEditing={this.props.onSubmitEditing}
          onEndEditing={this.props.onEndEditing}
          returnKeyType={this.props.returnKeyType}
          maxLength={this.props.maxLength}
          // 
          textContentType={this.props.password ? "password" : "none"}
        />
        {this.props.password &&
          (this.state.secureTextEntry ? (
            <Icon
              name={"visibility"}
              size={this.props.size}
              color={this.props.color}
              onPress={() => this.setState({ secureTextEntry: false })}
            />
          ) : (
              <Icon
                name={"visibility-off"}
                size={this.props.size}
                color={this.props.color}
                onPress={() => this.setState({ secureTextEntry: true })}
              />
            ))}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputWraper: {
    height: 65,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  icon: {
    justifyContent: "center",
    color: Colors.iconInput,
    fontSize: 25,
    alignSelf: "center"
  },
  textInput: {
    // ...Typography.mediumText,
    textAlign: "left",
    // marginLeft: 8,
    marginRight: 32,
    flex: 1,
    padding: 0
  },
  iconVisibility: {
    paddingLeft: 31,
    position: "absolute",
    right: 0,
    color: Colors.iconInput
  }
});
