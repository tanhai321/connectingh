import { Dimensions, PixelRatio } from "react-native";

let setting = {
    window: {
      pixelRatio: Number(PixelRatio.get().toFixed(2)),
      width: Number(Dimensions.get("window").width.toFixed(0)),
      height: Number(Dimensions.get("window").height.toFixed(0))
    },
  
    standardHeight: 800,
    standarWidth: 1280
  };
  console.log(
    'Dimensions.get("window")',
    Dimensions.get("window"),
    Dimensions.get("screen")
  );
  export class Layout {
    static get = () => {
      return setting;
    };
    static set = (width, height) => {
      setting = {
        ...setting,
        window: {
          ...setting.window,
          height: height,
          width: width
        }
      };
    };
    static widthScale = size => {
      const ratio = this.get().window.width / this.get().standarWidth;
      let resultSize = 0;
      if (size.toString().includes("%")) {
        const standarSize = (this.get().standarWidth * parseInt(size)) / 100;
        resultSize = ratio * standarSize;
      } else resultSize = ratio * size;
      return resultSize;
    };
  
    static heightScale = size => {
      const ratio = this.get().window.height / this.get().standardHeight;
      let resultSize = 0;
      if (size.toString().includes("%")) {
        const standarSize = (this.get().standardHeight * parseInt(size)) / 100;
        resultSize = ratio * standarSize;
      } else resultSize = ratio * size;
      return resultSize;
    };
  
    static moderateScale = (size, factor = 0.5) =>
      size + (this.width(size) - size) * factor;
  
    static minerScale = size => {
      const minerSize =
        this.get().window.width < this.get().window.height
          ? this.get().window.width
          : this.get().window.height;
      const minerStandarSize = this.get().standarWidth;
      const ratio = minerSize / minerStandarSize;
  
      let resultSize = 0;
      if (size.toString().includes("%")) {
        const standarSize = (minerStandarSize * parseInt(size)) / 100;
        resultSize = ratio * standarSize;
      } else resultSize = ratio * size;
  
      return resultSize;
    };
  }
  