import { create } from "apisauce";
import { BASE_URL, BASE_TOKEN } from "./URL";
export const API = create({
  baseURL: BASE_URL,
  headers: {
    "Content-Type": "application/json",
    "Cache-Control": "no-cache"
  },
});
